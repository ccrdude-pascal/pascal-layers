{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FireflyLayersTransportIndy10;

{$warn 5023 off : no warning about unused units}
interface

uses
  Layers.Transport.Indy10, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('FireflyLayersTransportIndy10', @Register);
end.
