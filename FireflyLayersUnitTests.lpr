program FireflyLayersUnitTests;

{$mode objfpc}{$H+}

uses
   Interfaces,
   Forms,
   GuiTestRunner,
   Layers.Transport.Synapse.TestCase,
   Layers.Internet.DNS.TestCase,
   Layers.Internet.DNS.Synapse.TestCase;

   {$R *.res}

begin
   Application.Initialize;
   Application.CreateForm(TGuiTestRunner, TestRunner);
   Application.Run;
end.
