unit Layers.Transport.Synapse.TestCase;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   fpcunit,
   testutils,
   testregistry,
   Windows,
   Layers.Transport.Base,
   Layers.Transport.Synapse;

type

   { TLayeredTransportSynapseTest }

   TLayeredTransportSynapseTest = class(TTestCase)
   protected
      procedure SetUp; override;
   published
      procedure Test_Class;
      procedure Test_GET_http;
      procedure Test_GET_https;
      procedure Test_GET_https_abupdate;
      procedure Test_GET_https_using_proxy;
   end;

implementation

procedure TLayeredTransportSynapseTest.Test_GET_http;
var
   trans: TLayeredTransport;
   ms: TMemoryStream;
begin
   trans := TLayeredTransport.CreateInstance;
   try
      ms := TMemoryStream.Create;
      try
         Self.AssertTrue(trans.GET('http://example.com/', ms));
         trans.DumpPersistentLogToEventLog;
      finally
         ms.Free;
      end;
   finally
      trans.Free;
   end;
end;

procedure TLayeredTransportSynapseTest.Test_GET_https;
var
   trans: TLayeredTransport;
   ms: TMemoryStream;
begin
   trans := TLayeredTransport.CreateInstance;
   try
      ms := TMemoryStream.Create;
      try
         Self.AssertTrue(trans.GET('https://example.com/', ms));
         trans.DumpPersistentLogToEventLog;
      finally
         ms.Free;
      end;
   finally
      trans.Free;
   end;
end;

procedure TLayeredTransportSynapseTest.Test_GET_https_abupdate;
var
   trans: TLayeredTransport;
   ms: TMemoryStream;
begin
   trans := TLayeredTransport.CreateInstance;
   try
      ms := TMemoryStream.Create;
      try
         Self.AssertTrue(trans.GET('https://www.safer-networking.org/api/version/1/?app=Spybot%20Anti-Beacon', ms));
         trans.DumpPersistentLogToEventLog;
      finally
         ms.Free;
      end;
   finally
      trans.Free;
   end;
end;

procedure TLayeredTransportSynapseTest.Test_GET_https_using_proxy;
var
   trans: TLayeredTransport;
   ms: TMemoryStream;
begin
   trans := TLayeredTransport.CreateInstance;
   try
      ms := TMemoryStream.Create;
      try
         trans.Proxy.LoadSystemProxy;
         Self.AssertTrue(trans.GET('https://www.icann.org/privacy/tos', ms));
         trans.DumpPersistentLogToEventLog;
      finally
         ms.Free;
      end;
   finally
      trans.Free;
   end;
end;

procedure TLayeredTransportSynapseTest.SetUp;
begin
   LayeredPickTransportSynapse;
end;

procedure TLayeredTransportSynapseTest.Test_Class;
var
   trans: TLayeredTransport;
begin
   trans := TLayeredTransport.CreateInstance;
   try
      CheckTrue(trans is TLayeredTransportSynapse);
   finally
      trans.Free;
   end;
end;


initialization

   RegisterTest(TLayeredTransportSynapseTest);
end.
