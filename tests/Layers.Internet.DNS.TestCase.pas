unit Layers.Internet.DNS.TestCase;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   fpcunit,
   Windows,
   testutils,
   testregistry,
   Layers.Internet.DNS.Base,
   Layers.Transport.Base,
   Layers.Transport.Synapse,
   Layers.JSON.FreePascal;

type

   { TTestCaseLayeredDNSoverHTTPS }

   TTestCaseLayeredDNSoverHTTPS = class(TTestCase)
   published
      procedure TestQuery;
      procedure TestCertificatePinning;
   end;

implementation

uses
   blcksock,
   ssl_openssl3;

procedure TTestCaseLayeredDNSoverHTTPS.TestQuery;
var
   dns: TLayeredDNSOverHTTPS;
   results: TLayeredDNSResults;
begin
   dns := TLayeredDNSOverHTTPS.Create(TLayeredTransport.CreateInstance);
   try
      results := TLayeredDNSResults.Create;
      try
         CheckTrue(dns.Query('ccrdude.net', results));
         CheckEquals(1, results.Answers.Count);
         CheckEquals('144.76.80.198', results.Answers[0].IP);
         dns.Transport.DumpPersistentLogToEventLog;
      finally
         results.Free;
      end;
   finally
      dns.Free;
   end;
end;

procedure TTestCaseLayeredDNSoverHTTPS.TestCertificatePinning;
var
   sock: TTCPBlockSocket;
   ssl: TSSLOpenSSL3;
   s: string;
begin
   sock := ttcpblocksocket.Create;
   sock.Connect('144.76.80.198', '443');
   if sock.LastError <> 0 then begin
      //memo1.Lines.Add(sock.LastErrorDesc);

      Exit;
   end;
   ssl := TSSLOpenSSL3.Create(sock);
   if ssl.LastError <> 0 then begin
      //memo1.Lines.Add(ssl.LastErrorDesc);

      Exit;
   end;
   ssl.Connect;
   if ssl.LastError <> 0 then begin
      //memo1.Lines.Add(ssl.LastErrorDesc);

      Exit;
   end;
   OutputDebugString(PChar(ssl.GetPeerIssuer));
   OutputDebugString(PChar(ssl.GetPeerFingerprint));
   OutputDebugString(PChar(ssl.GetCertInfo));
end;


initialization

   RegisterTest(TTestCaseLayeredDNSoverHTTPS);
end.
