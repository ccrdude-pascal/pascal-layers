unit Layers.Internet.DNS.Synapse.TestCase;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   fpcunit,
   testutils,
   testregistry,
   Layers.Internet.DNS.Base,
   Layers.Internet.DNS.Synapse;

type

   TTestCaseLayeredDNSSynapse = class(TTestCase)
   published
      procedure TestQuery;
   end;

implementation

procedure TTestCaseLayeredDNSSynapse.TestQuery;
var
   dns: TLayeredDNS;
   results: TLayeredDNSResults;
begin
   dns := TLayeredDNS.CreateInstance;
   try
      dns.ServerIP := '8.8.8.8';
      results := TLayeredDNSResults.Create;
      try
         CheckTrue(dns.Query('ccrdude.net', results));
         CheckEquals(1, results.Answers.Count);
         CheckEquals('144.76.80.198', results.Answers[0].IP);
      finally
         results.Free;
      end;
   finally
      dns.Free;
   end;
end;


initialization

   RegisterTest(TTestCaseLayeredDNSSynapse);
end.
