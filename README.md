
# Pascal Layers

Some units creating an additional compatibility layer for often used purposes.

## Why?

I sometimes write code that I want to use in FreePascal/Lazarus and in Delphi. There usually exist components that work fine on both, but some have issues. I always want the fastest.

These compatibility layers are published as a prerequisite for my Pascal units to access the Google Safe Browsing API (and some other coming API units).

## Features

* Compatibility layers 
  * JSON
  * HTTP(S)
 * Visual Connection Debugger

### JSON

JSON is supported using the following libraries:

* [fcl-json](https://wiki.freepascal.org/fcl-json) (FreePascal Component Library)
* [lkJSON](https://sourceforge.net/projects/lkjson/) (Leonin Koninins JSON Delphi Library), plus a special version for FreePascal.

### HTTP(S)

HTTP queries are supported using the following libraries:

* [Ararat Synapse](http://synapse.ararat.cz/) (OpenSSL 3.0)
* [Indy 10](https://www.indyproject.org/download/v10/) (OpenSSL 1.0 only)
* Might add the [Internet Component Suite](http://wiki.overbyte.eu/wiki/index.php/Main_Page) in the future (Delphi only).

### XML

Will follow in the future, with support for 
* native FreePascal and
* native Delphi components, 
* and probably Muetze's XMLLib.

### Encryption

An existing layer project for hashes and Ciphers, using DEC, DCrypt and FreePascals native algorithm implementations will be ported as well.
