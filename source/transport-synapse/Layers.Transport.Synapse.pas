{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Provides access to the Google Safe Browsing endpoints using Synapse.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-12-07  pk   5m  Support for new proxy fields
// 2023-03-16  pk  ---  Moved into separate tansport unit.
// *****************************************************************************
   )
}

unit Layers.Transport.Synapse;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   httpsend,
   {$IFDEF Windows}
   Windows,
   {$ENDIF Windows}
   Layers.Transport.Base,
   Layers.Transport.Debugger.List;

type

   { TDebugHTTPSend }

   TLayeredHTTPSend = class(THTTPSend)
   public
      function DebugHTTPMethod(const Method, URL: string): boolean;
   end;

   { TLayeredTransportSynapse }

   TLayeredTransportSynapse = class(TLayeredTransport)
   private
      FFollowRedirects: boolean;
      FInstance: TLayeredHTTPSend;
      FMaxRedirectSteps: integer;
   protected
      function GetHTTPInstance: TLayeredHTTPSend;
      function DoHTTPMethod(AHTTP: TLayeredHTTPSend; const Method, URL: string): boolean;
      function VerifyCert({%H-}Sender: TObject): boolean;
   public
      constructor Create; override;
      function POST(AURL: string; const AInput: TMemoryStream; const AResponse: TMemoryStream): boolean; override; overload;
      function PATCH(AURL: string; const AInput: TMemoryStream; const AResponse: TMemoryStream): boolean; override; overload;
      function GET(AURL: string; const AResponse: TMemoryStream): boolean; override;
      property FollowRedirects: boolean read FFollowRedirects write FFollowRedirects;
      property MaxRedirectSteps: integer read FMaxRedirectSteps write FMaxRedirectSteps;
   end;

procedure LayeredPickTransportSynapse;

implementation

uses
   ssl_openssl3;

procedure LayeredPickTransportSynapse;
begin
   TLayeredTransport.SetDefaultClass(TLayeredTransportSynapse);
end;

{ TDebugHTTPSend }

function TLayeredHTTPSend.DebugHTTPMethod(const Method, URL: string): boolean;
var
   conn: TLayeredTransportConnection;
begin
   conn := TLayeredTransportConnection.Create;
   conn.Method := UpperCase(Method);
   conn.URL := URL;
   conn.Request.Headers.Assign(Self.Headers);
   Self.Document.Seek(0, soFromBeginning);
   conn.Request.Data.CopyFrom(Self.Document, Self.Document.Size);
   Self.Document.Seek(0, soFromBeginning);
   Result := inherited HTTPMethod(Method, URL);
   conn.Code := Self.FResultCode;
   conn.Response.Headers.Assign(Self.Headers);
   Self.Document.Seek(0, soFromBeginning);
   conn.Response.Data.CopyFrom(Self.Document, Self.Document.Size);
   Self.Document.Seek(0, soFromBeginning);
   TLayeredTransportConnectionList.Instance.Add(conn);
end;

{ TLayeredTransportSynapse }

function TLayeredTransportSynapse.GetHTTPInstance: TLayeredHTTPSend;
var
   i: integer;
begin
   Result := TLayeredHTTPSend.Create;
   if Self.Proxy.Use then begin
      Result.ProxyHost := Self.Proxy.Host;
      Result.ProxyPort := Self.Proxy.Port;
      Result.ProxyUser := Self.Proxy.Username;
      Result.ProxyPass := Self.Proxy.Password;
   end;
   if Length(Self.ContentType) > 0 then begin
      Result.Headers.Add('Content-Type: ' + Self.ContentType);
   end;
   if Length(Self.UserAgent) > 0 then begin
      Result.Headers.Add('User-Agent: ' + Self.UserAgent);
   end;
   for i := 0 to Pred(Self.Headers.Count) do begin
      Result.Headers.Add(Self.Headers.Names[i] + ': ' + Self.Headers.ValueFromIndex[i]);
   end;
end;

function TLayeredTransportSynapse.DoHTTPMethod(AHTTP: TLayeredHTTPSend; const Method, URL: string): boolean;
var
   sURL: string;
   iAttempt: integer;
   iHeader: integer;
   bLocationFound: boolean;
begin
   FInstance := AHTTP;
   if VerifyCertificate then begin
      AHTTP.Sock.SSL.CertCAFile := ExtractFilePath(ParamStr(0)) + 'cacert.pem';
      AHTTP.Sock.SSL.VerifyCert := True;
      AHTTP.Sock.SSL.OnVerifyCert := VerifyCert;
   end;
   sURL := URL;
   iAttempt := 0;
   bLocationFound := true;
   repeat
      {$IFDEF DEBUG}
      Result := AHTTP.DebugHTTPMethod(Method, sURL);
      {$ELSE DEBUG}
      Result := AHTTP.HTTPMethod(Method, sURL);
      {$ENDIF DEBUG}
      Self.LogResult(Method, AHTTP.ResultCode, sURL);
      if (200 <> AHTTP.ResultCode) then begin
         OutputDebugString(PChar('TLayeredTransportSynapse.DoHTTPMethod: status code: ' + IntToStr(AHTTP.ResultCode)));
      end;
      if (301 = AHTTP.ResultCode) or (302 = AHTTP.ResultCode) then begin
         {$IFDEF Windows}
         OutputDebugString('TLayeredTransportSynapse.DoHTTPMethod: redirect detected...');
         {$ENDIF Windows}
         bLocationFound := false;
         for iHeader := 0 to Pred(AHTTP.Headers.Count) do begin
            if 'Location' = Copy(AHTTP.Headers[iHeader], 1, 8) then begin
               sURL := Trim(Copy(AHTTP.Headers[iHeader], 11, Length(AHTTP.Headers[iHeader]) - 10));
               OutputDebugString(PChar('TLayeredTransportSynapse.DoHTTPMethod: redirecting to ' + sURL));
               bLocationFound := true;
            end;
         end;
      end;
      Inc(iAttempt);
   until (not FFollowRedirects)
   or (not ((301 = AHTTP.ResultCode) or (302 = AHTTP.ResultCode)))
   or (iAttempt >= FMaxRedirectSteps)
   or (not bLocationFound);
   if AHTTP.ResultCode <> 200 then begin
      Inc(TLayeredTransport.PersistentNon200Count);
   end;
end;

function TLayeredTransportSynapse.VerifyCert(Sender: TObject): boolean;
var
   i: longint;
begin
   Result := True;
   try
      Self.CertificateInformation.CertInfo := FInstance.Sock.SSL.GetCertInfo;
      Self.CertificateInformation.CipherName := FInstance.Sock.SSL.GetCipherName;
      Self.CertificateInformation.PeerFingerprint := FInstance.Sock.SSL.GetPeerFingerprint;
      Self.CertificateInformation.PeerIssuer := FInstance.Sock.SSL.GetPeerIssuer;
      Self.CertificateInformation.PeerName := FInstance.Sock.SSL.GetPeerName;
      Self.CertificateInformation.PeerSubject := FInstance.Sock.SSL.GetPeerSubject;
      Self.CertificateInformation.SSLVersion := FInstance.Sock.SSL.GetSSLVersion;
      //ASN1_INTEGER_to_BN();
      i := FInstance.Sock.SSL.GetPeerSerialNo;
      Self.CertificateInformation.SerialNumber := IntToStr(i);

      Self.CertificateInformation.Available := True;
      Self.LogSSL(Self.CertificateInformation);
   except
      Self.CertificateInformation.Available := False;
   end;
end;

function TLayeredTransportSynapse.POST(AURL: string; const AInput: TMemoryStream; const AResponse: TMemoryStream): boolean;
var
   http: TLayeredHTTPSend;
begin
   http := Self.GetHTTPInstance;
   try
      http.Document.CopyFrom(AInput, AInput.Size);
      Result := Self.DoHTTPMethod(http, 'POST', AURL);
      http.Document.Seek(0, soFromBeginning);
      AResponse.CopyFrom(http.Document, http.Document.Size);
      FResultCode := http.ResultCode;
   finally
      http.Free;
   end;
end;

function TLayeredTransportSynapse.PATCH(AURL: string; const AInput: TMemoryStream; const AResponse: TMemoryStream): boolean;
var
   http: TLayeredHTTPSend;
begin
   http := Self.GetHTTPInstance;
   try
      http.Document.CopyFrom(AInput, AInput.Size);
      Result := Self.DoHTTPMethod(http, 'PATCH', AURL);
      http.Document.Seek(0, soFromBeginning);
      AResponse.CopyFrom(http.Document, http.Document.Size);
      FResultCode := http.ResultCode;
   finally
      http.Free;
   end;
end;

function TLayeredTransportSynapse.GET(AURL: string; const AResponse: TMemoryStream): boolean;
var
   http: TLayeredHTTPSend;
begin
   http := Self.GetHTTPInstance;
   try
      Result := Self.DoHTTPMethod(http, 'GET', AURL);
      http.Document.Seek(0, soFromBeginning);
      AResponse.CopyFrom(http.Document, http.Document.Size);
      FResultCode := http.ResultCode;
   finally
      http.Free;
   end;
end;

constructor TLayeredTransportSynapse.Create;
begin
   inherited Create;
   FFollowRedirects := True;
   FMaxRedirectSteps := 10;
   //Self.FUserAgent := 'Layers.Transport.Synapse/0.2';
end;

begin
   LayeredPickTransportSynapse;
end.
