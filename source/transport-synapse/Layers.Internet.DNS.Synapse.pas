unit Layers.Internet.DNS.Synapse;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Layers.Internet.DNS.Base;

type

   { TLayeredDNSSynapse }

   TLayeredDNSSynapse = class(TLayeredDNS)
   public
      function Query(ADomain: string; const TheResults: TLayeredDNSResults; AQueryType: string = 'A'): boolean; override;
   end;

procedure LayeredPickInternetDNSSynapse;

implementation

uses
   dnssend;

procedure LayeredPickInternetDNSSynapse;
begin
   TLayeredDNS.SetDefaultClass(TLayeredDNSSynapse);
end;

{ TLayeredDNSSynapse }

function TLayeredDNSSynapse.Query(ADomain:         string; const TheResults: TLayeredDNSResults; AQueryType: string): boolean;
var
   dns: TDNSSend;
   iType: integer;
   sl: TStringList;
   i: integer;
   a: TDNSAnswer;
begin
   dns := TDNSSend.Create;
   try
      dns.TargetHost := Self.ServerIP;
      if ('A' = AQueryType) then begin
         iType := QTYPE_A;
      end else if ('AAAA' = AQueryType) then begin
         iType := QTYPE_AAAA;
      end else begin
         iType := 0;
      end;
      sl := TStringList.Create;
      try
         Result := dns.DNSQuery(ADomain, iType, sl);
         for i := 0 to Pred(sl.Count) do begin
            a := TDNSAnswer.Create;
            a.IP := sl[i];
            a.AType := iType;
            TheResults.Answers.Add(a);
         end;
         //OutputDebugString(PChar(string(sl.Text)));
      finally
         sl.Free;
      end;
   finally
      dns.Free;
   end;
end;

begin
   LayeredPickInternetDNSSynapse;
end.
