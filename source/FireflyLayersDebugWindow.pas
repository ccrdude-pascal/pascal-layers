{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FireflyLayersDebugWindow;

{$warn 5023 off : no warning about unused units}
interface

uses
  Layers.Transport.Debugger.UI, Layers.Transport.Debugger.UIStreamFrame, 
  Layers.Transport.Debugger.UIFrame, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('FireflyLayersDebugWindow', @Register);
end.
