{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Implements JSON access using uLKJSON.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-16  pk  ---  Wrote interfaces necessary to read threatLists
// *****************************************************************************
   )
}

unit Layers.JSON.LeonKon;

{$IFDEF FPC}
{$mode Delphi}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   {$IFDEF FPC}
   // The standard library is not fully FPC compatible
   uLKJSON.FPC,
   {$ELSE FPC}
   // @link https://sourceforge.net/projects/lkjson/
   uLkJSON,
   {$ENDIF FPC}
   Layers.JSON.Base;

type

   { TLayeredJSONArrayLeonKon }

   TLayeredJSONArrayLeonKon = class(TLayeredJSONArray)
   protected
      FJSONArray: TlkJSONlist;
      FCallback: TLayeredJSONForEachObjectEvent;
      function GetCount: integer; override;
      function GetObjects(Index: integer): TLayeredJSONObject; override;
      procedure ForEachLKObject(ElName: string; Elem: TlkJSONbase; Data: pointer; var Continue: boolean);
   public
      constructor Create(ACreateObject: boolean); override;
      procedure AddString(AValue: string); override;
      procedure AddObject(AnObject: TLayeredJSONObject); override;
      procedure ForEachObject(ACallbackObject: TLayeredJSONForEachObjectEvent; AnUserData: pointer); override;
      property JSONArray: TlkJSONlist read FJSONArray;
   end;

   { TLayeredJSONObjectLeonKon }

   TLayeredJSONObjectLeonKon = class(TLayeredJSONObject)
   protected
      FJSONObject: TlkJSONobject;
   public
      constructor Create(ACreateObject: boolean); override;
      procedure FreeObject; override;
      procedure ToStream(const AStream: TMemoryStream); override;
      procedure FromStream(const AStream: TMemoryStream); override;
      function FindArray(AKey: string; out AnArray: TLayeredJSONArray): boolean; override;
      function FindObject(AKey: string; out AnObject: TLayeredJSONObject): boolean; override;
      function FindString(AKey: string; out AString: string): boolean; override;
      function FindInteger(AKey: string; out AValue: integer): boolean; override;
      procedure AddString(AKey, AValue: string); override;
      procedure AddObject(AKey: string; AnObject: TLayeredJSONObject); override;
      procedure AddArray(AKey: string; AnObject: TLayeredJSONArray); override;
      property JSONObject: TlkJSONobject read FJSONObject write FJSONObject;
   end;

procedure LayeredPickJSONLeonKon;

implementation

procedure LayeredPickJSONLeonKon;
begin
   TLayeredJSONArray.SetDefaultClass(TLayeredJSONArrayLeonKon);
   TLayeredJSONObject.SetDefaultClass(TLayeredJSONObjectLeonKon);
end;

{ TLayeredJSONArrayLeonKon }

function TLayeredJSONArrayLeonKon.GetCount: integer;
begin
   Result := Self.FJSONArray.Count;
end;

function TLayeredJSONArrayLeonKon.GetObjects(Index: integer): TLayeredJSONObject;
begin
   Result := TLayeredJSONObjectLeonKon.Create(False);
   TLayeredJSONObjectLeonKon(Result).FJSONObject := Self.JSONArray.Child[Index] as TlkJSONObject;
end;

procedure TLayeredJSONArrayLeonKon.ForEachLKObject(ElName: string; Elem: TlkJSONbase; Data: pointer; var Continue: boolean);
var
   o: TLayeredJSONObject;
begin
   if Elem.SelfType = jsObject then begin
      o := TLayeredJSONObject.CreateInstance(False);
      try
         TLayeredJSONObjectLeonKon(o).JSONObject := Elem as TlkJSONObject;
         FCallback(ElName, o, Data, Continue);
      finally
         o.Free;
      end;
   end;
end;

constructor TLayeredJSONArrayLeonKon.Create(ACreateObject: boolean);
begin
   if ACreateObject then begin
      Self.FJSONArray := TlkJSONList.Create;
   end else begin
      Self.FJSONArray := nil;
   end;
end;

procedure TLayeredJSONArrayLeonKon.AddString(AValue: string);
begin
   Self.FJSONArray.Add(AValue);
end;

procedure TLayeredJSONArrayLeonKon.AddObject(AnObject: TLayeredJSONObject);
begin
   Self.FJSONArray.Add(TLayeredJSONObjectLeonKon(AnObject).JSONObject);
end;

procedure TLayeredJSONArrayLeonKon.ForEachObject(ACallbackObject: TLayeredJSONForEachObjectEvent; AnUserData: pointer);
begin
   FCallback := ACallbackObject;
   Self.JSONArray.ForEach(ForEachLKObject, AnUserData);
end;

{ TLayeredJSONObjectLeonKon }

constructor TLayeredJSONObjectLeonKon.Create(ACreateObject: boolean);
begin
   Self.FOwnsObject := ACreateObject;
   if ACreateObject then begin
      Self.FJSONObject := TlkJSONObject.Create;
   end else begin
      Self.FJSONObject := nil;
   end;
end;

procedure TLayeredJSONObjectLeonKon.FreeObject;
begin
   FreeAndNil(Self.FJSONObject);
end;

procedure TLayeredJSONObjectLeonKon.ToStream(const AStream: TMemoryStream);
begin
   TlkJSONstreamed.SaveToStream(Self.JSONObject, AStream);
end;

procedure TLayeredJSONObjectLeonKon.FromStream(const AStream: TMemoryStream);
var
   d: TlkJSONbase;
begin
   d := TlkJSONstreamed.LoadFromStream(AStream);
   if d.SelfType = jsObject then begin
      Self.FJSONObject := d as TlkJSONObject;
   end else begin
      d.Free;
      raise LayeredJSONParsingException.Create('Data is not a JSON object');
   end;
end;

function TLayeredJSONObjectLeonKon.FindArray(AKey: string; out AnArray: TLayeredJSONArray): boolean;
var
   i: integer;
begin
   AnArray := nil;
   i := Self.FJSONObject.IndexOfName(WideString(AKey));
   Result := (i > -1);
   if not Result then begin
      Exit;
   end;
   if Self.FJSONObject.FieldByIndex[i].SelfType <> jsList then begin
      Result := False;
      Exit;
   end;
   try
      AnArray := TLayeredJSONArrayLeonKon.Create(False);
      TLayeredJSONArrayLeonKon(AnArray).FJSONArray := Self.FJSONObject.FieldByIndex[i] as TlkJSONList;
      Result := True;
   except
      Result := False;
   end;
end;

function TLayeredJSONObjectLeonKon.FindObject(AKey: string; out AnObject: TLayeredJSONObject): boolean;
var
   i: integer;
begin
   AnObject := nil;
   i := Self.FJSONObject.IndexOfName(WideString(AKey));
   Result := (i > -1);
   if not Result then begin
      Exit;
   end;
   try
      AnObject := TLayeredJSONObjectLeonKon.Create(False);
      TLayeredJSONObjectLeonKon(AnObject).FJSONObject := Self.FJSONObject.FieldByIndex[i] as TlkJSONObject;
      Result := True;
   except
      Result := False;
   end;
end;

function TLayeredJSONObjectLeonKon.FindString(AKey: string; out AString: string): boolean;
var
   i: integer;
begin
   AString := '';
   i := Self.FJSONObject.IndexOfName(WideString(AKey));
   Result := (i > -1);
   if not Result then begin
      Exit;
   end;
   try
      AString := (Self.FJSONObject.FieldByIndex[i] as TlkJSONstring).Value;
      Result := True;
   except
      Result := False;
   end;
end;

function TLayeredJSONObjectLeonKon.FindInteger(AKey: string; out AValue: integer): boolean;
var
   i: integer;
begin
   AValue := 0;
   i := Self.FJSONObject.IndexOfName(WideString(AKey));
   Result := (i > -1);
   if not Result then begin
      Exit;
   end;
   try
      AValue := (Self.FJSONObject.FieldByIndex[i] as TlkJSONnumber).Value;
      Result := True;
   except
      Result := False;
   end;
end;

procedure TLayeredJSONObjectLeonKon.AddString(AKey, AValue: string);
begin
   Self.FJSONObject.Add(WideString(AKey), AValue);
end;

procedure TLayeredJSONObjectLeonKon.AddObject(AKey: string; AnObject: TLayeredJSONObject);
begin
   Self.FJSONObject.Add(WideString(AKey), TLayeredJSONObjectLeonKon(AnObject).JSONObject);
end;

procedure TLayeredJSONObjectLeonKon.AddArray(AKey: string; AnObject: TLayeredJSONArray);
begin
   Self.FJSONObject.Add(WideString(AKey), TLayeredJSONArrayLeonKon(AnObject).JSONArray);
end;

begin
   LayeredPickJSONLeonKon;
end.
