{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Provides access to the Google Safe Browsing endpoints using Iny 10.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-12-07  pk   5m  Support for new proxy fields
// 2023-03-16  pk  ---  Moved into separate tansport unit.
// *****************************************************************************
   )
}

unit Layers.Transport.Indy10;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   IdHTTP,
   IdSSLOpenSSL,
   Layers.Transport.Base,
   Layers.Transport.Debugger.List;

type

   { TLayeredIdHTTP }

   TLayeredIdHTTP = class(TIdHTTP)
   public
      procedure DebugPost(AURL: string; ASource, AResponseContent: TStream);
      procedure DebugGet(AURL: string; AResponseContent: TStream);
   end;

   { TLayeredTransportIndy10 }

   TLayeredTransportIndy10 = class(TLayeredTransport)
   protected
      procedure DoPost(AHTTP: TLayeredIdHTTP; AURL: string; ASource, AResponseContent: TStream);
      procedure DoGet(AHTTP: TLayeredIdHTTP; AURL: string; AResponseContent: TStream);
      function GetHTTPInstance: TLayeredIdHTTP;
   public
      constructor Create; override;
      function GET(AURL: string; const AResponse: TMemoryStream): boolean; override;
      function POST(AURL: string; const AInput: TMemoryStream; const AResponse: TMemoryStream): boolean; override; overload;
   end;

procedure LayeredPickTransportIndy10;

implementation

procedure LayeredPickTransportIndy10;
begin
   TLayeredTransport.SetDefaultClass(TLayeredTransportIndy10);
end;

{ TLayeredIdHTTP }

procedure TLayeredIdHTTP.DebugPost(AURL: string; ASource, AResponseContent: TStream);
var
   conn: TLayeredTransportConnection;
begin
   conn := TLayeredTransportConnection.Create;
   conn.Method := 'POST';
   conn.URL := AURL;
   ASource.Seek(0, soFromBeginning);
   conn.Request.Data.CopyFrom(ASource, ASource.Size);
   ASource.Seek(0, soFromBeginning);
   Self.Post(AURL, ASource, AResponseContent);
   conn.Code := Self.ResponseCode;
   conn.Request.Headers.Assign(Self.Request.RawHeaders);
   conn.Response.Headers.Assign(Self.Response.RawHeaders);
   AResponseContent.Seek(0, soFromBeginning);
   conn.Response.Data.CopyFrom(AResponseContent, AResponseContent.Size);
   AResponseContent.Seek(0, soFromBeginning);
   TLayeredTransportConnectionList.Instance.Add(conn);
end;

procedure TLayeredIdHTTP.DebugGet(AURL: string; AResponseContent: TStream);
var
   conn: TLayeredTransportConnection;
begin
   conn := TLayeredTransportConnection.Create;
   conn.Method := 'GET';
   conn.URL := AURL;
   Self.Get(AURL, AResponseContent);
   conn.Code := Self.ResponseCode;
   conn.Request.Headers.Assign(Self.Request.RawHeaders);
   conn.Response.Headers.Assign(Self.Response.RawHeaders);
   AResponseContent.Seek(0, soFromBeginning);
   conn.Response.Data.CopyFrom(AResponseContent, AResponseContent.Size);
   AResponseContent.Seek(0, soFromBeginning);
   TLayeredTransportConnectionList.Instance.Add(conn);
end;

{ TLayeredTransportIndy10 }

procedure TLayeredTransportIndy10.DoPost(AHTTP: TLayeredIdHTTP; AURL: string; ASource, AResponseContent: TStream);
begin
   {$IFDEF DEBUG}
   AHTTP.DebugPost(AURL, ASource, AResponseContent);
   {$ELSE DEBUG}
   AHTTP.Post(AURL, ASource, AResponseContent);
   {$ENDIF DEBUG}
end;

procedure TLayeredTransportIndy10.DoGet(AHTTP: TLayeredIdHTTP; AURL: string; AResponseContent: TStream);
begin
   {$IFDEF DEBUG}
   AHTTP.DebugGET(AURL, AResponseContent);
   {$ELSE DEBUG}
   AHTTP.GET(AURL, AResponseContent);
   {$ENDIF DEBUG}
end;

function TLayeredTransportIndy10.GetHTTPInstance: TLayeredIdHTTP;
begin
   Result := TLayeredIdHTTP.Create;
   if Self.Proxy.Use then begin
      Result.ProxyParams.ProxyServer := Self.Proxy.Host;
      Result.ProxyParams.ProxyPort := StrToIntDef(Self.Proxy.Port, 8888);
      Result.ProxyParams.ProxyUsername := Self.Proxy.Username;
      Result.ProxyParams.ProxyPassword := Self.Proxy.Password;
   end;
   Result.Request.ContentType := Self.ContentType;
   Result.Request.UserAgent := Self.UserAgent;
   Result.IOHandler := TIdSSLIOHandlerSocketOpenSSL.Create;
end;

function TLayeredTransportIndy10.POST(AURL: string; const AInput: TMemoryStream; const AResponse: TMemoryStream): boolean;
var
   http: TLayeredIdHTTP;
begin
   http := GetHTTPInstance;
   try
      try
         Self.DoPost(http, AURL, AInput, AResponse);
         Result := True;
      except
         Result := False;
      end;
   finally
      http.IOHandler.Free;
      http.IOHandler := nil;
      http.Free;
   end;
end;

function TLayeredTransportIndy10.GET(AURL: string; const AResponse: TMemoryStream): boolean;
var
   http: TLayeredIdHTTP;
begin
   http := GetHTTPInstance;
   try
      try
         Self.DoGet(http, AURL, AResponse);
         Result := True;
      except
         Result := False;
      end;
   finally
      http.IOHandler.Free;
      http.IOHandler := nil;
      http.Free;
   end;
end;

constructor TLayeredTransportIndy10.Create;
begin
   inherited Create;
   Self.FUserAgent := 'Layers.Transport.Indy10/0.1';
end;

begin
   LayeredPickTransportIndy10;
end.
