{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Provides access to the Google Safe Browsing endpoints using ICS.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-16  pk  ---  Moved into separate tansport unit.
// *****************************************************************************
   )
}

unit Layers.Transport.ICS;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Layers.Transport.Base;

type

   {
      TLayeredTransportICS implements a http transport mechanism using
      the Internet Components Suite.

      This needs to be implemented and tested in Delphi, since it is not
      FreePascal compatible.

      @link http://wiki.overbyte.eu/wiki/index.php/ICS_Download#Download_OpenSSL_Binaries_.28required_for_SSL-enabled_components.29
   }
   TLayeredTransportICS = class(TLayeredTransport)
   protected
      function POST(AURL: string; const AInput: TMemoryStream; const AResponse: TMemoryStream): boolean; override; overload;
      function GET(AURL: string; const AResponse: TMemoryStream): boolean; override;
   end;

procedure LayeredPickTransportICS;

implementation

procedure LayeredPickTransportICS;
begin
   LayeredTransportClass := TLayeredTransportICS;
end;

{ TLayeredTransportICS }

function TLayeredTransportICS.POST(AURL: string; const AInput: TMemoryStream; const AResponse: TMemoryStream): boolean;
begin
   // TODO : Implement ICS transport
end;

function TLayeredTransportICS.GET(AURL: string; const AResponse: TMemoryStream): boolean;
begin
   // TODO : Implement ICS transport
end;

begin
   LayeredPickTransportICS;
end.
