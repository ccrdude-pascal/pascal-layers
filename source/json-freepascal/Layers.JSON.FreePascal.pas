{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Implements JSON access using FreePascal libraries.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-16  pk  ---  Wrote interfaces necessary to read threatLists
// *****************************************************************************
   )
}

unit Layers.JSON.FreePascal;

{$IFDEF FPC}
{$mode Delphi}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   fpjson,
   jsonparser,
   Layers.JSON.Base;

type

   { TLayeredJSONArrayFreePascal }

   TLayeredJSONArrayFreePascal = class(TLayeredJSONArray)
   protected
      FJSONArray: TJSONArray;
      function GetCount: integer; override;
      function GetObjects(Index: integer): TLayeredJSONObject; override;
   public
      constructor Create(ACreateObject: boolean); override;
      procedure FromStream(const AStream: TMemoryStream); override;
      procedure AddString(AValue: string); override;
      procedure AddObject(AnObject: TLayeredJSONObject); override;
      procedure ForEachObject(ACallback: TLayeredJSONForEachObjectEvent; AnUserData: pointer); override;
      procedure GetStrings(AList: TStrings); override;
      property JSONArray: TJSONArray read FJSONArray write FJSONArray;
   end;

   { TLayeredJSONObjectFreePascal }

   TLayeredJSONObjectFreePascal = class(TLayeredJSONObject)
   protected
      FJSONObject: TJSONObject;
   public
      constructor Create(ACreateObject: boolean); override;
      procedure FreeObject; override;
      destructor Destroy; override;
      procedure ToStream(const AStream: TMemoryStream; ACompact: boolean); override;
      procedure FromStream(const AStream: TMemoryStream); override;
      function FindArray(AKey: string; out AnArray: TLayeredJSONArray): boolean; override;
      function FindBoolean(AKey: string; out AValue: boolean): boolean; override;
      function FindInteger(AKey: string; out AValue: integer): boolean; override;
      function FindInt64(AKey: string; out AValue: int64): boolean; override;
      function FindObject(AKey: string; out AnObject: TLayeredJSONObject): boolean; override;
      function FindString(AKey: string; out AString: string): boolean; override;
      procedure AddArray(AKey: string; AnObject: TLayeredJSONArray); override;
      procedure AddBoolean(AKey: string; AValue: boolean); override;
      procedure AddInt64(AKey: string; AValue: int64); override;
      procedure AddObject(AKey: string; AnObject: TLayeredJSONObject); override;
      procedure AddString(AKey, AValue: string); override;
      property JSONObject: TJSONObject read FJSONObject write FJSONObject;
   end;

procedure Register;
procedure LayeredPickJSONFreePascal;

implementation

procedure Register;
begin
   // nothing to register here
end;

procedure LayeredPickJSONFreePascal;
begin
   TLayeredJSONArray.SetDefaultClass(TLayeredJSONArrayFreePascal);
   TLayeredJSONObject.SetDefaultClass(TLayeredJSONObjectFreePascal);
end;

{ TLayeredJSONArrayFreePascal }

function TLayeredJSONArrayFreePascal.GetCount: integer;
begin
   Result := Self.FJSONArray.Count;
end;

function TLayeredJSONArrayFreePascal.GetObjects(Index: integer): TLayeredJSONObject;
begin
   Result := TLayeredJSONObjectFreePascal.Create(False);
   TLayeredJSONObjectFreePascal(Result).JSONObject := Self.JSONArray.Objects[Index];
end;

constructor TLayeredJSONArrayFreePascal.Create(ACreateObject: boolean);
begin
   if ACreateObject then begin
      Self.FJSONArray := TJSONArray.Create;
   end else begin
      Self.FJSONArray := nil;
   end;
end;

procedure TLayeredJSONArrayFreePascal.FromStream(const AStream: TMemoryStream);
var
   d: TJSONData;
begin
   d := GetJSON(AStream, True);
   if d.JSONType = jtArray then begin
      Self.FJSONArray := d as TJSONArray;
   end else begin
      d.Free;
      raise LayeredJSONParsingException.Create('Data is not a JSON array');
   end;
end;

procedure TLayeredJSONArrayFreePascal.AddString(AValue: string);
begin
   Self.FJSONArray.Add(AValue);
end;

procedure TLayeredJSONArrayFreePascal.AddObject(AnObject: TLayeredJSONObject);
begin
   Self.FJSONArray.Add(TLayeredJSONObjectFreePascal(AnObject).JSONObject);
end;

procedure TLayeredJSONArrayFreePascal.ForEachObject(ACallback: TLayeredJSONForEachObjectEvent; AnUserData: pointer);
var
   i: integer;
   o: TLayeredJSONObject;
   bContinue: boolean;
begin
   bContinue := True;
   for i := 0 to Pred(Self.Count) do begin
      try
         o := Self.Objects[i];
         try
            ACallback('', o, AnUserData, bContinue);
         finally
            o.Free;
         end;
         if not bContinue then begin
            break;
         end;
      except
         on E: Exception do begin
            // TODO : log
         end;
      end;
   end;
end;

procedure TLayeredJSONArrayFreePascal.GetStrings(AList: TStrings);
var
   i: integer;
begin
   for i := 0 to Pred(Self.Count) do begin
      try
         AList.Add(Self.FJSONArray.Strings[i]);
      except
         on E: Exception do begin
            // TODO : log
         end;
      end;
   end;
end;

{ TLayeredJSONObjectFreePascal }

constructor TLayeredJSONObjectFreePascal.Create(ACreateObject: boolean);
begin
   Self.FOwnsObject := ACreateObject;
   if ACreateObject then begin
      Self.FJSONObject := TJSONObject.Create;
   end else begin
      Self.FJSONObject := nil;
   end;
end;

procedure TLayeredJSONObjectFreePascal.FreeObject;
begin
   FreeAndNil(Self.FJSONObject);
end;

destructor TLayeredJSONObjectFreePascal.Destroy;
begin
   inherited Destroy;
end;

procedure TLayeredJSONObjectFreePascal.ToStream(const AStream: TMemoryStream; ACompact: boolean);
var
   ss: TStringStream;
begin
   if ACompact then begin
      Self.FJSONObject.DumpJSON(AStream);
   end else begin
      ss := TStringStream.Create(Self.FJSONObject.FormatJSON());
      try
         ss.Position := 0;
         AStream.CopyFrom(ss, ss.Size);
      finally
         ss.Free;
      end;
   end;
end;

procedure TLayeredJSONObjectFreePascal.FromStream(const AStream: TMemoryStream);
var
   d: TJSONData;
begin
   d := GetJSON(AStream, True);
   if d.JSONType = jtObject then begin
      Self.FJSONObject := d as TJSONObject;
   end else begin
      d.Free;
      raise LayeredJSONParsingException.Create('Data is not a JSON object');
   end;
end;

function TLayeredJSONObjectFreePascal.FindArray(AKey: string; out AnArray: TLayeredJSONArray): boolean;
var
   a: TJSONArray;
begin
   Result := Self.JSONObject.Find(AKey, a);
   if Result then begin
      AnArray := TLayeredJSONArrayFreePascal.Create(False);
      TLayeredJSONArrayFreePascal(AnArray).JSONArray := a;
   end;
end;

function TLayeredJSONObjectFreePascal.FindObject(AKey: string; out AnObject: TLayeredJSONObject): boolean;
var
   o: TJSONObject;
begin
   Result := Self.JSONObject.Find(AKey, o);
   if Result then begin
      AnObject := TLayeredJSONObjectFreePascal.Create(False);
      TLayeredJSONObjectFreePascal(AnObject).JSONObject := o;
   end;
end;

function TLayeredJSONObjectFreePascal.FindString(AKey: string; out AString: string): boolean;
var
   s: TJSONString;
begin
   Result := Self.JSONObject.Find(AKey, s);
   if Result then begin
      AString := s.AsString;
   end;
end;

function TLayeredJSONObjectFreePascal.FindInteger(AKey: string; out AValue: integer): boolean;
var
   n: TJSONNumber;
begin
   Result := Self.JSONObject.Find(AKey, n);
   if Result then begin
      AValue := n.AsInteger;
   end;
end;

function TLayeredJSONObjectFreePascal.FindInt64(AKey: string; out AValue: int64): boolean;
var
   n: TJSONNumber;
begin
   Result := Self.JSONObject.Find(AKey, n);
   if Result then begin
      case n.NumberType of
         ntInt64: begin
            AValue := n.AsInt64;
         end;
         ntQWord: begin
            AValue := n.AsQWord;
         end;
         ntInteger: begin
            AValue := n.AsInteger;
         end;
      end;
   end;
end;

function TLayeredJSONObjectFreePascal.FindBoolean(AKey: string; out AValue: boolean): boolean;
var
   b: TJSONBoolean;
begin
   Result := Self.JSONObject.Find(AKey, b);
   if Result then begin
      AValue := b.AsBoolean;
   end;
end;

procedure TLayeredJSONObjectFreePascal.AddInt64(AKey: string; AValue: int64);
begin
   Self.FJSONObject.Add(AKey, AValue);
end;

procedure TLayeredJSONObjectFreePascal.AddString(AKey, AValue: string);
begin
   Self.FJSONObject.Add(AKey, AValue);
end;

procedure TLayeredJSONObjectFreePascal.AddObject(AKey: string; AnObject: TLayeredJSONObject);
begin
   Self.FJSONObject.Add(AKey, TLayeredJSONObjectFreePascal(AnObject).JSONObject);
end;

procedure TLayeredJSONObjectFreePascal.AddArray(AKey: string; AnObject: TLayeredJSONArray);
begin
   Self.FJSONObject.Add(AKey, TLayeredJSONArrayFreePascal(AnObject).JSONArray);
end;

procedure TLayeredJSONObjectFreePascal.AddBoolean(AKey: string; AValue: boolean);
begin
   Self.FJSONObject.Add(AKey, AValue);
end;

begin
   LayeredPickJSONFreePascal;
end.
