unit Layers.Transport.Foundation;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   CocoaAll,
   Layers.Transport.Base,
   Layers.Transport.Debugger.List;

type

   { TLayeredTransportSynapse }

   TLayeredTransportSynapse = class(TLayeredTransport)
   public
      function GET(AURL: string; const AResponse: TMemoryStream): boolean; override;
   end;

implementation

{ TLayeredTransportSynapse }

function TLayeredTransportSynapse.GET(AURL: string; const AResponse: TMemoryStream): boolean;
var
   url: NSURL;
   sURL: NSString;
   r: NSURLRequest;
   resp: NSHTTPURLResponse;
   e: NSErrorPointer;
   d: NSData;
   iStatus: NSInteger;
   ms: TMemoryStream;
begin
   FStatusCode := 0;
   FStatusText := '';
   try
      //if AIsEncoded then begin
      sURL := NSSTR(PChar(AURL)); // .stringByAddingPercentEscapesUsingEncoding(NSASCIIStringEncoding);
      //end else begin
      //   sURL := NSSTR(PChar(AURL)).stringByAddingPercentEscapesUsingEncoding(NSASCIIStringEncoding);
      //end;
      url := NSURL(NSURL.URLWithString(sURL));
      r := NSURLRequest(NSURLRequest.requestWithURL(url));
      d := NSURLConnection.sendSynchronousRequest_returningResponse_error(r, @resp, nil);
      iStatus := resp.statusCode;
      FStatusCode := iStatus;
      FStatusText += #13#10'Status: ' + IntToStr(iStatus);
      FStatusText += #13#10'Content Size: ' + IntToStr(d.length);
      ms := TMemoryStream.Create;
      try
         ms.SetSize(d.length);
         //FErrorMessage += #13#10'6';
         System.Move(d.bytes^, ms.Memory^, d.length);
         //FErrorMessage += #13#10'7';
         ms.Seek(0, soFromBeginning);
         AResponse.CopyFrom(ms, ms.Size);
      finally
         ms.Free;
      end;
      Result := True;
      //FErrorMessage += #13#10'9';
   except
      on E: Exception do begin
         Result := False;
         FStatusText += E.Message;
      end;
   end;
end;

end.
{ TMacOsFoundationClientDownloader }{$IFDEF UseFoundation}(*
function TMacOsFoundationClientDownloader.Download(AURL: string; AStream: TStream; AIsEncoded: boolean): boolean;
var
   url: NSURL;
   sURL: NSString;
   r: NSURLRequest;
   resp: NSHTTPURLResponse;
   e: NSErrorPointer;
   d: NSData;
   iStatus: NSInteger;
   ms: TMemoryStream;
begin
   FStatusCode := 0;
   FStatusText := '';
   try
      if AIsEncoded then begin
         sURL := NSSTR(PChar(AURL)); // .stringByAddingPercentEscapesUsingEncoding(NSASCIIStringEncoding);
      end else begin
         sURL := NSSTR(PChar(AURL)).stringByAddingPercentEscapesUsingEncoding(NSASCIIStringEncoding);
      end;
      url := NSURL(NSURL.URLWithString(sURL));
      r := NSURLRequest(NSURLRequest.requestWithURL(url));
      d := NSURLConnection.sendSynchronousRequest_returningResponse_error(r, @resp, nil);
      iStatus := resp.statusCode;
      FStatusCode := iStatus;
      FStatusText += #13#10'Status: ' + IntToStr(iStatus);
      FStatusText += #13#10'Content Size: ' + IntToStr(d.length);
      ms := TMemoryStream.Create;
      try
         ms.SetSize(d.length);
         //FErrorMessage += #13#10'6';
         System.Move(d.bytes^, ms.Memory^, d.length);
         //FErrorMessage += #13#10'7';
         ms.Seek(0, soFromBeginning);
         AStream.CopyFrom(ms, ms.Size);
      finally
         ms.Free;
      end;
      Result := True;
      //FErrorMessage += #13#10'9';
   except
      on E: Exception do begin
         Result := False;
         FStatusText += E.Message;
      end;
   end;
end;
*){$ENDIF UseFoundation}
