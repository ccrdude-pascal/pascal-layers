{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Provides a frame to display Internet communication details.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-17  pk  ---  Created as part of connection debugger.
// *****************************************************************************
   )
}

unit Layers.Transport.Debugger.UIStreamFrame;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   ComCtrls,
   ValEdit,
   SynEdit,
   khexeditor,
   synhighlighterjscript,
   Layers.Transport.Debugger.List;

type

   { TFrameConnectionStream }

   TFrameConnectionStream = class(TFrame)
      hex: TKHexEditor;
      PageControl1: TPageControl;
      edit: TSynEdit;
      tabHeaders: TTabSheet;
      tabText: TTabSheet;
      tabRaw: TTabSheet;
      ValueListEditor1: TValueListEditor;
   private
      FData: TLayeredTransportConnectionData;
      FHighlighterJavascript: TSynJScriptSyn;
      procedure SetData(AValue: TLayeredTransportConnectionData);
   public
      constructor Create(TheOwner: TComponent); override;
      property Data: TLayeredTransportConnectionData read FData write SetData;
   end;

implementation

{$R *.lfm}

{ TFrameConnectionStream }

procedure TFrameConnectionStream.SetData(AValue: TLayeredTransportConnectionData);
var
   i: integer;
   sContentType: string;
begin
   FData := AValue;
   FData.Data.Position := 0;
   edit.Lines.LoadFromStream(FData.Data);
   FData.Data.Position := 0;
   hex.LoadFromStream(FData.Data);
   ValueListEditor1.Clear;
   for i := 0 to Pred(FData.ParsedHeaders.Count) do begin
      ValueListEditor1.Values[FData.ParsedHeaders.Names[i]] := FData.ParsedHeaders.ValueFromIndex[i];
   end;
   sContentType := FData.ContentType;
   if (sContentType = 'application/json') then begin
      edit.Highlighter := Self.FHighlighterJavascript;
   end else begin
      edit.Highlighter := nil;
   end;
end;

constructor TFrameConnectionStream.Create(TheOwner: TComponent);
begin
   inherited Create(TheOwner);
   Self.FHighlighterJavascript := TSynJScriptSyn.Create(Self);
end;

end.
