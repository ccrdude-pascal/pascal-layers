{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Provides a forme to display Internet communication details.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-17  pk  ---  Created as part of connection debugger.
// *****************************************************************************
   )
}

unit Layers.Transport.Debugger.UI;

{$mode Delphi}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ComCtrls,
   ExtCtrls,
   Layers.Transport.Debugger.UIStreamFrame;

type

  { TFormTransportDebugger }

  TFormTransportDebugger = class(TForm)
     lv: TListView;
     PageControl1: TPageControl;
     Splitter1: TSplitter;
     tabContent: TTabSheet;
     procedure FormShow({%H-}Sender: TObject);
     procedure lvData({%H-}Sender: TObject; Item: TListItem);
     procedure lvSelectItem({%H-}Sender: TObject; Item: TListItem; Selected: Boolean);
  private
     FRequest: TFrameConnectionStream;
     FResponse: TFrameConnectionStream;
     procedure DoListUpdated({%H-}Sender: TObject);
  public
     constructor Create(TheOwner: TComponent); override;
     procedure Refresh;
  private
     class var FInstance: TFormTransportDebugger;
  public
     class constructor Create;
     class destructor Destroy;
     class function Instance: TFormTransportDebugger;
  end;

implementation

uses
   OVM.ListView,
   Layers.Transport.Debugger.List;

{$R *.lfm}

{ TFormTransportDebugger }

procedure TFormTransportDebugger.lvData(Sender: TObject; Item: TListItem);
var
   conn: TLayeredTransportConnection;
begin
   conn := TLayeredTransportConnectionList.Instance[Item.Index];
   if lv.Columns.Count = 0 then begin
      lv.UpdateColumnsForClass(conn);
   end;
   lv.UpdateListItemForObject(Item, conn);
end;

procedure TFormTransportDebugger.lvSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
var
   conn: TLayeredTransportConnection;
begin
   if not Selected then begin
      Exit;
   end;
   if not Assigned(Item) then begin
      Exit;
   end;
   conn := TLayeredTransportConnection(Item.Data);
   Self.FRequest.Data := conn.Request;
   Self.FResponse.Data := conn.Response;
end;

procedure TFormTransportDebugger.DoListUpdated(Sender: TObject);
begin
   Self.Refresh;
end;

constructor TFormTransportDebugger.Create(TheOwner: TComponent);
begin
   inherited Create(TheOwner);
   FRequest := nil;
   FResponse := nil;
   TLayeredTransportConnectionList.Instance.RegisterListener(DoListUpdated);
end;

procedure TFormTransportDebugger.FormShow(Sender: TObject);
begin
   Refresh;
end;

class constructor TFormTransportDebugger.Create;
begin
   FInstance := nil;
end;

class destructor TFormTransportDebugger.Destroy;
begin
   FInstance.Free;
end;

class function TFormTransportDebugger.Instance: TFormTransportDebugger;
begin
   if not Assigned(FInstance) then begin
     FInstance := TFormTransportDebugger.Create(nil);
   end;
   Result := FInstance;
end;

procedure TFormTransportDebugger.Refresh;
var
   i: integer;
   li: TListItem;
   conn: TLayeredTransportConnection;
begin
   if not Assigned(FRequest) then begin
      FRequest := TFrameConnectionStream.Create(Self);
      FRequest.Name := '';
      FRequest.Parent := tabContent;
      FRequest.Align := alTop;
      FRequest.Height := 200;
   end;
   if not Assigned(FResponse) then begin
      FResponse := TFrameConnectionStream.Create(Self);
      FResponse.Name := '';
      FResponse.Parent := tabContent;
      FResponse.Align := alClient;
   end;
   //lv.Items.Count := TLayeredTransportConnectionList.Instance.Count;
   for i := lv.Items.Count to Pred(TLayeredTransportConnectionList.Instance.Count) do begin
      conn := TLayeredTransportConnectionList.Instance[i];
      if lv.Columns.Count = 0 then begin
         lv.UpdateColumnsForClass(conn);
      end;
      li := lv.Items.Insert(0);
      lv.UpdateListItemForObject(li, conn);
   end;
   lv.Selected := lv.Items[0];
   lv.Refresh;
end;

end.

