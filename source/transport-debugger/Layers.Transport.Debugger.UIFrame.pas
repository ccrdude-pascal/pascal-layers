unit Layers.Transport.Debugger.UIFrame;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ComCtrls,
   ExtCtrls,
   Layers.Transport.Debugger.UIStreamFrame;

type

  { TFrameTransportDebugger }

  TFrameTransportDebugger = class(TFrame)
    lv: TListView;
    PageControl1: TPageControl;
    Splitter1: TSplitter;
    tabContent: TTabSheet;
    procedure lvData({%H-}Sender: TObject; Item: TListItem);
    procedure lvSelectItem({%H-}Sender: TObject; Item: TListItem; Selected: Boolean);
  private
     FRequest: TFrameConnectionStream;
     FResponse: TFrameConnectionStream;
     procedure DoListUpdated({%H-}Sender: TObject);
  public
     constructor Create(TheOwner: TComponent); override;
     procedure Refresh;
  end;

implementation

{$R *.lfm}

uses
   OVM.ListView,
   Layers.Transport.Debugger.List;

{$R *.lfm}

{ TFrameTransportDebugger }

procedure TFrameTransportDebugger.lvData(Sender: TObject; Item: TListItem);
var
   conn: TLayeredTransportConnection;
begin
   conn := TLayeredTransportConnectionList.Instance[Item.Index];
   if lv.Columns.Count = 0 then begin
      lv.UpdateColumnsForClass(conn);
   end;
   lv.UpdateListItemForObject(Item, conn);
end;

procedure TFrameTransportDebugger.lvSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
var
   conn: TLayeredTransportConnection;
begin
   if not Selected then begin
      Exit;
   end;
   if not Assigned(Item) then begin
      Exit;
   end;
   conn := TLayeredTransportConnection(Item.Data);
   Self.FRequest.Data := conn.Request;
   Self.FResponse.Data := conn.Response;
end;

procedure TFrameTransportDebugger.DoListUpdated(Sender: TObject);
begin
   Self.Refresh;
end;

constructor TFrameTransportDebugger.Create(TheOwner: TComponent);
begin
   inherited Create(TheOwner);
   FRequest := nil;
   FResponse := nil;
   TLayeredTransportConnectionList.Instance.RegisterListener(DoListUpdated);
end;

procedure TFrameTransportDebugger.Refresh;
var
   i: integer;
   li: TListItem;
   conn: TLayeredTransportConnection;
begin
   if not Assigned(FRequest) then begin
      FRequest := TFrameConnectionStream.Create(Self);
      FRequest.Name := '';
      FRequest.Parent := tabContent;
      FRequest.Align := alTop;
      FRequest.Height := 200;
   end;
   if not Assigned(FResponse) then begin
      FResponse := TFrameConnectionStream.Create(Self);
      FResponse.Name := '';
      FResponse.Parent := tabContent;
      FResponse.Align := alClient;
   end;
   //lv.Items.Count := TLayeredTransportConnectionList.Instance.Count;
   for i := lv.Items.Count to Pred(TLayeredTransportConnectionList.Instance.Count) do begin
      conn := TLayeredTransportConnectionList.Instance[i];
      if lv.Columns.Count = 0 then begin
         lv.UpdateColumnsForClass(conn);
      end;
      li := lv.Items.Insert(0);
      lv.UpdateListItemForObject(li, conn);
   end;
   lv.Selected := lv.Items[0];
   lv.Refresh;
end;

end.

