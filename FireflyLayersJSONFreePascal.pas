{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FireflyLayersJSONFreePascal;

{$warn 5023 off : no warning about unused units}
interface

uses
  Layers.JSON.FreePascal, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('Layers.JSON.FreePascal', @Layers.JSON.FreePascal.Register);
end;

initialization
  RegisterPackage('FireflyLayersJSONFreePascal', @Register);
end.
